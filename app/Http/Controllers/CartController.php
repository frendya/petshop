<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

use App\Produk;

class CartController extends Controller
{
    public function index()
    {
      $cartItems = Cart::content();
      return view ('cart.index', compact('cartItems'));
    }

    public function addItem($id)
    {
      $produks = Produk::find($id);

      Cart::add($id, $produks->judul, 1, $produks->harga);
      return back();
    }
    public function destroy($id)
    {
      Cart::remove($id);
      return back();
    }
}
