<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class HomeController extends Controller
{
    protected $produks;
    //public function __construct()
    //{
      //$this->middleware('auth');
    //}

    public function index()
    {
      $produks = Produk::paginate(1);
      return view('front.shop', ['produks' => $produks ]);
    }
    public function shop()
    {
      $produks = Produk::paginate(3);
      return view('front.shop', compact('produks'));
    }

    public function produk_detail($id)
    {
      $produks = Produk::where('id', $id);

      return view('front.produk_detail', compact('produks'), ['produks', $produks]);
    }

    public function contact()
    {
      return view('front.contact');
    }
    public function search(Request $request)
    {
      $search = $request->get('search_data');
      if($search==""){
        return view('front.home');
      }
      else{
      $produks = Produk::where('judul', 'LIKE', '%'.$search.'%')->paginate(2);
      return view('front.shop',  ['msg' => 'Result: ' .$search],  compact('produks', 'search'));
    }
  }
}
