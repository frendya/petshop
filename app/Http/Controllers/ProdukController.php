<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Produk;
use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use Storage;
use Illuminate\Support\Facades\Input;
use File;
use Validator;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    //public function __construct()
    //{
    //  $this->middleware('auth');
    //}

    public function getIndex()
    {
        $produks = Produk::all();
        return view('front.home');
    }
    public function getAddToCart(Request $request, $id)
    {
        $produk = Produk::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($produk, $produk->id);

        $request->Session()->put('cart', $cart);
        return redirect()->route('produk.index');
    }
    public function getCart() {
      if (!Session::has('cart'))
      {
          return view('shop.shopping-cart');
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);
      return view('shop.shopping-cart', ['produks' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function index()
    {
      //tampilkan data record
      $produks = Produk::all();
      return view('crud.index', ['produks' => $produks ]);
    }
    public function create()
    {
        //masuk halaman create data baru
        return view('crud.create');
    }

    public function store(Request $request)
    {
        //validasi
        $this->validate($request,[
          'judul' => 'required',
          'ras' => 'required',
          'deskripsi' => 'required',
          'harga' => 'required',
          'image' => 'required|image|mimes:png,jpg,jpeg,gif|max:2048 '
        ]);

        //create new data
        $produks = new Produk();

        $produks->judul = $request->judul;
        $produks->ras = $request->ras;
        $produks->deskripsi = $request->deskripsi;
        $produks->harga = $request->harga;

        //upload file gambar baru
        $file = $request->file('image');
                $filename = $file->getClientOriginalName();
                $request->file('image')->move("images/", $filename);
                $produks->imagePath = $filename;

          $produks->save();
          return redirect()->route('crud.index')->with('alert-success','Data Berhasil Disimpan!');

    }

    public function show($id)
    {
      //
    }

    public function edit($id)
    {
        $produks = Produk::findOrFail($id);
        //masuk halaman edit view
        return view('crud.edit', compact('produks'));
    }

    public function update(Request $request, $id)
    {

        $update = Produk::where('id', $id)->first();
        $update->judul = $request['judul'];
        $update->ras = $request['ras'];
        $update->deskripsi = $request['deskripsi'];
        $update->harga = $request['harga'];


        if($request->file('image') == "")
        {
            $update->imagePath = $update->imagePath;
        }
        else
        {
            File::delete('images/'.$update->imagePath);
            $file       = $request->file('image');
            $fileName   = $file->getClientOriginalName();
            $request->file('image')->move("images/", $fileName);
            $update->imagePath = $fileName;
        }

        $update->update();
        return redirect()->route('crud.index');
  }

    public function destroy($id)
    {
        //delete Data
        $produks = Produk::findOrFail($id);
        $produks->delete();
        return redirect()->route('crud.index')->with('alert-success','Data Berhasil Dihapus!');
    }
}
