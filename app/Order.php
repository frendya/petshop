<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = 'orders';

protected $fillable = array('user_id','alamat','total');

public function orderItems()
  {
      return $this->belongsToMany('Produk') ->withPivot('amount','total');
  }
}
