<?php

use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produks')->delete();
        $produk = new \App\Produk([
          'imagePath' => 'http://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg',
          'judul' => 'Kucing Himalaya',
          'ras' => 'Himalaya',
          'deskripsi' => 'Kucing ini adalah keturunan Himalaya yang berumur 6 bulan.',
          'harga' => 500000
        ]);
        $produk->save();

        $produk = new \App\Produk([
          'imagePath' => 'http://d39kbiy71leyho.cloudfront.net/wp-content/uploads/2016/01/07182338/Olivia-Benson-600x283.png',
          'judul' => 'Kucing Scottist',
          'ras' => 'Scottist',
          'deskripsi' => 'Kucing yang menarik.',
          'harga' => 900000
        ]);
        $produk->save();

        $produk = new \App\Produk([
          'imagePath' => 'http://www.recordingweekly.com/wp-content/uploads/2014/05/grumpy-cat-breed-type.jpg',
          'judul' => 'Kucing Persia',
          'ras' => 'Persia',
          'deskripsi' => 'Kucing dengan ras persia baru lahir sekitar 3 bulan lalu.',
          'harga' => 700000
        ]);
        $produk->save();

        $produk = new \App\Produk([
          'imagePath' => 'http://animalia-life.club/data_images/american-curl/american-curl6.jpg',
          'judul' => 'Kucing American-curl',
          'ras' => 'American-curl',
          'deskripsi' => 'American-Curl dibawa langsung dari America dan di kembangbiakan di Indonesia',
          'harga' => 1200000
        ]);
        $produk->save();

        $produk = new \App\Produk([
          'imagePath' => 'http://animalia-life.club/data_images/russian-blue/russian-blue9.jpg',
          'judul' => 'Kucing Russian-blue',
          'ras' => 'Russian-blue',
          'deskripsi' => 'Russian-blue adalah kucing yang berhabibat asli di negara Rusia',
          'harga' => 2500000
        ]);
        $produk->save();
    }
}
