@extends('adminlte::layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-md-12">
      <h1>CRUD produk</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
				<div class="panel-body">
      <form class="" action="{{route('crud.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group{{ ($errors->has('image')) ? $errors->first('image') : '' }}">
          Gambar <input type="file" name="image" class="form-control" placeholder="Masukan File Gambar">
          {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('judul')) ? $errors->first('judul') : '' }}">
          <input type="text" name="judul" class="form-control" placeholder="Masukan Judul">
          {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('ras')) ? $errors->first('ras') : '' }}">
          <input type="text" name="ras" class="form-control" placeholder="Masukan ras">
          {!! $errors->first('ras', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('deskripsi')) ? $errors->first('deskripsi') : '' }}">
          <textarea class="form-control" name="deskripsi" rows="5" placeholder="Masukan deskripsi"></textarea>
          {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('harga')) ? $errors->first('harga') : '' }}">
          <input type="text" name="harga" class="form-control" placeholder="Masukan harga">
          {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" value="save">
        </div>
      </form>
    </div>
  </div>
    </div>
  </div>
@endsection
