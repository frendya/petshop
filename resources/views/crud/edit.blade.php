@extends('adminlte::layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-md-12">
      <h1>Edit Data</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
				<div class="panel-body">
      <form class="" action="{{route('crud.update', $produks->id)}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PATCH">
        {{csrf_field()}}
        <div class="form-group{{ ($errors->has('image')) ? $errors->first('image') : '' }}">
          <input type="file" name="image" class="form-control" placeholder="Masukan imagePath" value="{{$produks->imagePath}}"/>
          {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('judul')) ? $errors->first('judul') : '' }}">
          <input type="text" name="judul" class="form-control" placeholder="Masukan Judul" value="{{$produks->judul}}"/>
          {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('ras')) ? $errors->first('ras') : '' }}">
          <input type="text" name="ras" class="form-control" placeholder="Masukan ras" value="{{$produks->ras}}"/>
          {!! $errors->first('ras', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('deskripsi')) ? $errors->first('deskripsi') : '' }}">
          <input type="texarea" class="form-control" name="deskripsi" rows="5" placeholder="Masukan deskripsi" value="{{$produks->deskripsi}}"/>
          {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ ($errors->has('harga')) ? $errors->first('harga') : '' }}">
          <input type="text" name="harga" class="form-control" placeholder="Masukan harga" value="{{$produks->harga}}"/>
          {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-primary" value="save">
        </div>
      </form>
    </div>
  </div>
    </div>
  </div>
@endsection
