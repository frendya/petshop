@extends('adminlte::layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-md-12">
      <h1>CRUD produk</h1>
    </div>
  </div>
  <div class="row">
    <table class="table table-striped">
      <tr>
        <th>No.</th>
        <th>Judul</th>
        <th>Ras</th>
        <th>Deskripsi</th>
        <th>Harga</th>
        <th>imagePath</th>
        <th>Actions</th>
      </tr>

      <a href="{{route('crud.create')}}" class="btn btn-info pull-right">Tambah Data</a><br>
      <?php $no=1; ?>

      @foreach($produks as $produk)
          <tr>
            <td>{{$no++}}</td>
            <td>{{$produk->judul}}</td>
            <td>{{$produk->ras}}</td>
            <td>{{$produk->deskripsi}}</td>
            <td>{{$produk->harga}}</td>
            <td><img src="{{ url('images/', $produk->imagePath)  }}" style="max-height:50px;max-width:50px;margin-top:10px;" alt=""></td>
            <td>
                <form class="" action="{{ route('crud.destroy',$produk->id) }}" method="post">
                <input type="hidden" name="_method" value="delete">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <a href="{{route('crud.edit', $produk->id)}}" class="btn btn-primary">Edit</a>
                <input type="submit" class="btn btn-danger" onclick="return confirm('Apakah anda yakin menghapus data ini?');" name="name" value="delete">
                </form>
            </td>
        </tr>
        @endforeach


    </table>
  </div>
@endsection
