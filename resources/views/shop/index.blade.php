@extends('layouts.master')
@section('title')
  Laravel Shopping Cart
@endsection

@section('content')
  @foreach($produks->chunk(3) as $produkChunk)
    <div class="row">
      @foreach($produkChunk as $produk)
        <div class="col-sm-6 col-md-4 image-holder">
          <div class="thumbnail">
            <img src="{{ url('images', $produk->imagePath)}}" alt="..." class="img-responsive">
            <div class="caption">
              <h3>{{ $produk->judul }}</h3>
              <p class="desciption">{{ $produk->deskripsi }}</p>
              <div class="clearfix">
                <div class="pull-left harga">RP {{ $produk->harga }}</div>
              <p><a href="{{ route('produk.addToCart', ['id' => $produk->id])}}" class="btn btn-success pull-right" role="button">Add to Cart</a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      @endforeach
@endsection
