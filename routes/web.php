<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
  'uses' => 'ProdukController@getIndex',
  'as' => 'produk.index'
]);

Route::get('/produk', function (){
  return view('front.shop');
});

Route::get('/home','HomeController@index');
Route::get('/shop','HomeController@shop');
Route::get('/contact','HomeController@contact');
Route::post('/search','HomeController@search');
Route::get('/produk_detail/{id}', 'HomeController@produk_detail');
Route::get('/cart','CartController@index');

Route::get('/cart/addItem/{id}', 'CartController@addItem');
Route::get('/cart/remove/{id}', 'CartController@destroy');


Route::auth();
Route::get('/admin', 'AdminController@index');

/*
Route::post('/signup', [
  'uses' => 'RegisterController@postSignup',
  'as' => 'user.signup'
]);
*/

Route::get('/add-to-cart/{id}', [
  'uses' => 'ProdukController@getAddToCart',
  'as' => 'produk.addToCart'
]);

Route::get('/shopping-cart', [
  'uses' => 'ProdukController@getCart',
  'as' => 'produk.shoppingCart'
]);


Route::resource('crud','ProdukController');
Route::get('file','ProdukController@index');
Route::post('store', 'ProdukController@store');
Route::get('edit','ProdukController@edit');

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
